package be.contribute.atlassian.condition;

import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.repository.ref.restriction.*;
import com.atlassian.bitbucket.web.conditions.AbstractPermissionCondition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sun.awt.AWTAccessor;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public class CanTagCondition extends AbstractPermissionCondition {
    private final RepositoryService repositoryService;
    private final RefRestrictionService refRestrictionService;

    private static final Logger log = LoggerFactory.getLogger(CanTagCondition.class);

    public CanTagCondition(PermissionService permissionService, RefRestrictionService refRestrictionService, RepositoryService repositoryService) {
        super(permissionService, false);
        this.repositoryService = repositoryService;
        this.refRestrictionService = refRestrictionService;
    }


    @Override
    protected boolean hasPermission(Map<String, Object> context) {
        ServletRequest request = (ServletRequest) context.get("request");
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        if (httpServletRequest != null) {
            String path = httpServletRequest.getServletPath();
            Repository repository = extractRepository(path);
            String commitHash = extractCommitHash(path);
            if (repository != null) {
                if (permissionService.hasRepositoryPermission(repository, getPermission())) {

                    SimpleTag tag = new SimpleTag.Builder().hash(commitHash).latestCommit(commitHash).id("refs/tags/simple-tag").displayId("Simple Tag").build();
                    RefAccessRequest createRefAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.CREATE).ref(tag).build();
                    RefAccessRequest updateRefAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.UPDATE).ref(tag).build();
                    RefAccessRequest deleteRefAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.DELETE).ref(tag).build();
                    log.debug("Create Permission: " + Boolean.toString(refRestrictionService.hasPermission(createRefAccessRequest)));
                    log.debug("Update Permission: " + Boolean.toString(refRestrictionService.hasPermission(updateRefAccessRequest)));
                    log.debug("Delete Permission: " + Boolean.toString(refRestrictionService.hasPermission(deleteRefAccessRequest)));
                    if (refRestrictionService.hasPermission(createRefAccessRequest) || refRestrictionService.hasPermission(updateRefAccessRequest) || refRestrictionService.hasPermission(deleteRefAccessRequest)) {
                        log.debug("User has permission!");
                        return true;
                    } else {
                        log.debug("User has repo permission, but doesn't have branch permission!");
                        return false;
                    }
                }
            }
        } else {
            return false;
        }
        return false;
    }


    private Repository extractRepository(String pathInfo) {
        String[] strings = pathInfo.split("/");
        String projectKey = null;
        String reposSlug = null;
        for (int i = 0; i < strings.length; i++) {
            String currentPath = strings[i];
            if (i != 0) {
                String lastPath = strings[i - 1];
                if ("projects".equals(lastPath) && currentPath != null) {
                    projectKey = strings[i];
                }
                if ("users".equals(lastPath) && currentPath != null) {
                    projectKey = "~" + strings[i];
                }
                if ("repos".equals(lastPath) && currentPath != null) {
                    reposSlug = strings[i];
                }
            }
        }
        return getRepository(projectKey, reposSlug);
    }

    private String extractCommitHash(String pathInfo) {
        String[] strings = pathInfo.split("/");
        String projectKey = null;
        String reposSlug = null;
        String commitHash = null;
        for (int i = 0; i < strings.length; i++) {
            String currentPath = strings[i];
            if (i != 0) {
                String lastPath = strings[i - 1];
                if ("commits".equals(lastPath) && currentPath != null) {
                    commitHash = strings[i];
                }
            }
        }
        return commitHash;
    }


    private Repository getRepository(String projectKey, String repoSlug) {
        log.debug("Fetching repository with projectKey " + projectKey + " and repoSlug " + repoSlug);
        Repository repository = null;
        try {
            repository = repositoryService.getBySlug(projectKey, repoSlug);
        } catch (Exception e) {
            log.debug("error while fetching repository", e);
        }
        return repository;
    }
}
