package be.contribute.atlassian.tag.rest;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UpdateTagRestResource {
    @JsonProperty
    private String repoSlug;
    @JsonProperty
    private String originalTagName;
    @JsonProperty
    private String projectKey;
    @JsonProperty
    private String name;
    @JsonProperty
    private String message;
    @JsonProperty
    private String commitId;

    public String getRepoSlug() {
        return repoSlug;
    }

    public void setRepoSlug(String repoSlug) {
        this.repoSlug = repoSlug;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    public String getOriginalTagName() {
        return originalTagName;
    }

    public void setOriginalTagName(String originalTagName) {
        this.originalTagName = originalTagName;
    }

    public boolean renamed() {
        return !originalTagName.equals(name);
    }
}
