package be.contribute.atlassian.tag.rest;


import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.enums.RefType;
import be.contribute.atlassian.enums.TagType;
import com.atlassian.bitbucket.avatar.AvatarRequest;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.repository.*;
import com.atlassian.bitbucket.rest.commit.RestCommit;
import com.atlassian.bitbucket.rest.repository.RestTag;
import com.atlassian.bitbucket.util.DateFormatter;
import com.atlassian.bitbucket.rest.user.RestPerson;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

public class DetailedTagRestTransformFunction implements Function<DetailedTag, DetailedTagRestResource> {
    private static final int MIN_SIZE = 32;
    private static final Logger log = LoggerFactory.getLogger(DetailedTagRestTransformFunction.class);

    private final NavBuilder navBuilder;
    private final CommitService commitService;
    private final AvatarService avatarService;
    private final RefService refService;
    private final DateFormatter dateFormatter;

    private Repository repository;

    public DetailedTagRestTransformFunction(NavBuilder navBuilder, CommitService commitService, AvatarService avatarService, RefService refService, DateFormatter dateFormatter) {
        this.navBuilder = navBuilder;
        this.commitService = commitService;
        this.avatarService = avatarService;
        this.refService = refService;
        this.dateFormatter = dateFormatter;
    }

    public Repository getRepository() {
        return repository;
    }

    public NavBuilder getNavBuilder() {
        return navBuilder;
    }

    public CommitService getCommitService() {
        return commitService;
    }

    public AvatarService getAvatarService() {
        return avatarService;
    }

    public RefService getRepositoryService() {
        return refService;
    }

    public DateFormatter getDateFormatter() {
        return dateFormatter;
    }

    public void setRepository(Repository repository) {
        this.repository = repository;
    }

    @Nullable
    @Override
    public DetailedTagRestResource apply(@Nullable DetailedTag detailedTag) {
        log.debug("Trying to transform this tag: " + detailedTag.toString());
        checkNotNull(repository);
        DetailedTagRestResource resource = new DetailedTagRestResource();

        String linkUrl = navBuilder.project(repository.getProject()).repo(repository).browse().atRevision("refs/tags/" + detailedTag.getTagName()).buildAbsolute();
        resource.setSelf(linkUrl);


        resource.setType(detailedTag.getTagType().name());
        resource.setName(detailedTag.getTagName());

        String formattedDate = getTaggedOn(detailedTag);
        resource.setTaggedOn(formattedDate);

        resource.setDescription(detailedTag.getDescription());

        Tagger tagger = detailedTag.getTagger();
        if (tagger != null) {
            RestPerson restPerson = new RestPerson(tagger);
            String avatarUrl = avatarService.getUrlForPerson(tagger, new AvatarRequest(true, MIN_SIZE));
            restPerson.setAvatarUrl(avatarUrl);
            resource.setTagger(restPerson);
        }

        if (StringUtils.isBlank(detailedTag.getTaggedTagName()) && StringUtils.isBlank(detailedTag.getCommitHash())) {
            resource.setPointsToType(RefType.NONE);
            log.debug("This tag points to nothing...");
            log.debug(detailedTag.getCommitHash());
            log.debug(detailedTag.getPointsTo());
            log.debug(detailedTag.getResolvedTagId());
            log.debug(detailedTag.getSignature());
            log.debug(detailedTag.getSignatureVersion());
            log.debug(detailedTag.getTaggedTagName());
            log.debug(detailedTag.getTagName());
        //    log.debug(detailedTag.getTaggedDateTime().toString());
            log.debug(detailedTag.getDescription().toString());
            log.debug(detailedTag.getTagger().getName());
            log.debug(detailedTag.getTagType().name());
        }
        else if (StringUtils.isBlank(detailedTag.getTaggedTagName())) {
            CommitRequest request = new CommitRequest.Builder(repository, detailedTag.getCommitHash()).build();
            Commit commit = commitService.getCommit(request);
            RestCommit restCommit = new RestCommit(commit);
            String commitUrl = navBuilder.project(repository.getProject())
                    .repo(repository)
                    .commit(detailedTag.getCommitHash())
                    .buildAbsolute();
            restCommit.put("self", commitUrl);
            resource.setCommit(restCommit);
            resource.setSelf(commitUrl);
            resource.setPointsToType(RefType.COMMIT);
            resource.setPointsTo(commit.getDisplayId());
            log.debug("This tag points to a commit: " + restCommit.toString());
        } else {
            log.debug("This tag is the case of a filled TaggedTagName and a filled CommitHash");
            Tag tag = (Tag) refService.resolveRef(repository, detailedTag.getTaggedTagName());
            RestTag restTag = null;
            if (tag == null) {
                restTag = RestTag.EXAMPLE;
                restTag.put("hash", "");
                restTag.put("displayId", detailedTag.getTaggedTagName());
                restTag.put("id", detailedTag.getTaggedTagName());
                restTag.put("latestCommit", "");
                restTag.put("self", null);
            } else {
                restTag = new RestTag(tag);
                String tagUrl = navBuilder.project(repository.getProject()).repo(repository).browse().atRevision("refs/tags/" + detailedTag.getTaggedTagName()).buildAbsolute();
                restTag.put("self", tagUrl);
            }
            resource.setPointedTag(restTag);
            resource.setPointsTo(detailedTag.getTaggedTagName());
            resource.setPointsToType(RefType.TAG);
        }
        return resource;
    }

    private String getTaggedOn(DetailedTag detailedTag) {
        return detailedTag.getTaggedDateTime() == null ? "" : dateFormatter.formatDate(detailedTag.getTaggedDateTime().toDate(), DateFormatter.FormatType.LONG);

    }
}
