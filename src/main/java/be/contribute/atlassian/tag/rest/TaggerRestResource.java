package be.contribute.atlassian.tag.rest;


import be.contribute.atlassian.Tagger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TaggerRestResource {
    @JsonProperty
    private String name;
    @JsonProperty
    private String email;

    public TaggerRestResource(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public TaggerRestResource(Tagger tagger){
        this.name = tagger.getName();
        this.email = tagger.getEmail();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
