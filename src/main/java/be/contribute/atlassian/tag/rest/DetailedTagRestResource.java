package be.contribute.atlassian.tag.rest;

import be.contribute.atlassian.enums.RefType;
import com.atlassian.bitbucket.rest.commit.RestCommit;
import com.atlassian.bitbucket.rest.repository.RestTag;
import com.atlassian.bitbucket.rest.user.RestPerson;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DetailedTagRestResource {
    @JsonProperty
    private RestPerson tagger;
    @JsonProperty
    private String name;
    @JsonProperty
    private String[] description;
    @JsonProperty
    private String commitId;
    @JsonProperty
    private String type;
    @JsonProperty
    private String taggedOn;
    @JsonProperty
    private String self;
    @JsonProperty
    private RestCommit commit;
    @JsonProperty
    private RefType pointsToType;
    @JsonProperty
    private RestTag pointedTag;
    @JsonProperty
    private String pointsTo;

    public DetailedTagRestResource() {
    }

    public RestPerson getTagger() {
        return tagger;
    }

    public void setTagger(RestPerson tagger) {
        this.tagger = tagger;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getDescription() {
        return description;
    }

    public void setDescription(String[] description) {
        this.description = description;
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTaggedOn() {
        return taggedOn;
    }

    public void setTaggedOn(String taggedOn) {
        this.taggedOn = taggedOn;
    }

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public RestCommit getCommit() {
        return commit;
    }

    public void setCommit(RestCommit commit) {
        this.commit = commit;
    }

    public RefType getPointsToType() {
        return pointsToType;
    }

    public void setPointsToType(RefType pointsToType) {
        this.pointsToType = pointsToType;
    }

    public RestTag getPointedTag() {
        return pointedTag;
    }

    public void setPointedTag(RestTag pointedTag) {
        this.pointedTag = pointedTag;
    }

    public String getPointsTo() {
        return pointsTo;
    }

    public void setPointsTo(String pointsTo) {
        this.pointsTo = pointsTo;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(tagger.getName());
        sb.append(tagger.getEmailAddress());
        sb.append(name);
        sb.append(commitId);
        sb.append(type);
        sb.append(taggedOn);
        sb.append(self);
        //sb.append(commit.toString());
        //sb.append(commit.keySet());
        sb.append(pointsToType);
        sb.append(pointedTag);
        sb.append(pointsTo);
        return sb.toString();
    }
}
