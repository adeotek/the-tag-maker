package be.contribute.atlassian.tag.rest;


import com.atlassian.bitbucket.repository.Repository;

public interface TagRestResourceTransformationFactory {
    DetailedTagRestTransformFunction getTransformation(Repository repository);
}
