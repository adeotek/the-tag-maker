package be.contribute.atlassian.enums;


public enum TagType {
    ANNOTATED,
    SIGNED,
    LIGHTWEIGHT;
}
