package be.contribute.atlassian.enums;

/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 24/07/14
 * Time: 13:29
 * To change this template use File | Settings | File Templates.
 */
public enum DescriptionStatus {
    TO_COME,
    READING,
    TYPE_SIGNATURE,
    SIGNATURE,
    MAYBE_DONE,
    DONE;
}
