package be.contribute.atlassian;

import com.atlassian.bitbucket.user.Person;

public class Tagger implements Person {
    private String name;
    private String email;

    public Tagger(String name, String email) {
        this.name = name;
        this.email = email;
    }

    @Override
    public String getEmailAddress() {
        return email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
