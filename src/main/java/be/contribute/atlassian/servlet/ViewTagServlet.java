package be.contribute.atlassian.servlet;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.service.RepositoryResolver;
import be.contribute.atlassian.service.TagService;
import be.contribute.atlassian.tag.rest.DetailedTagRestResource;
import be.contribute.atlassian.tag.rest.TagRestResourceTransformationFactory;
import com.atlassian.bitbucket.repository.SimpleTag;
import com.atlassian.bitbucket.repository.ref.restriction.RefAccessRequest;
import com.atlassian.bitbucket.repository.ref.restriction.RefAccessType;
import com.atlassian.bitbucket.repository.ref.restriction.RefRestrictionService;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import com.atlassian.bitbucket.server.ApplicationPropertiesService;
import com.atlassian.bitbucket.permission.Permission;
import com.atlassian.bitbucket.permission.PermissionService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequestImpl;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class ViewTagServlet extends AbstractTagServlet {

    private static final Logger log = LoggerFactory.getLogger(ViewTagServlet.class);
    private static final int TAGS_PER_PAGE_LIMIT = 20;

    private final TagService tagService;
    private final PageBuilderService pageBuilderService;
    private final TagRestResourceTransformationFactory tagRestResourceTransformationFactory;
    private final PermissionService permissionService;
    private final RefRestrictionService refRestrictionService;

    public ViewTagServlet(SoyTemplateRenderer soyTemplateRenderer, RepositoryService repositoryService, RepositoryResolver resolver, TagService tagService, PageBuilderService pageBuilderService, TagRestResourceTransformationFactory tagRestResourceTransformationFactory, ApplicationPropertiesService applicationPropertiesService, PermissionService permissionService, RefRestrictionService refRestrictionService) {
        super(soyTemplateRenderer, repositoryService, resolver, applicationPropertiesService);
        this.tagService = tagService;
        this.pageBuilderService = pageBuilderService;
        this.tagRestResourceTransformationFactory = tagRestResourceTransformationFactory;
        this.permissionService = permissionService;
        this.refRestrictionService = refRestrictionService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        DetailedTag detailedTag = null;
        HashMap<String, Object> paramsMap = new HashMap<String, Object>();

        String repoSlug = req.getParameter("repo");
        String projectKey = req.getParameter("projKey");
        Repository repository = repositoryResolver.resolveRepository(projectKey, repoSlug, Boolean.parseBoolean(req.getParameter("personal")));

        int startingFrom = getStartingFrom(req);

        paramsMap.put("repository", repository);
        log.debug("Repository set up: " + repository.getSlug());
        paramsMap.put("startingFrom", startingFrom);
        paramsMap.put("baseUrl", getBaseUrl().toString());
        PageRequestImpl pageRequest = new PageRequestImpl(startingFrom, TAGS_PER_PAGE_LIMIT);
        Page<DetailedTag> tags = tagService.getTags(repository, pageRequest);
        log.debug("We found " + tagService.getTags(repository, pageRequest).getSize() + " tags.");

        Iterator<DetailedTag> iterator = tags.getValues().iterator();
        if (iterator != null) {
            if (iterator.hasNext()) {
                detailedTag = iterator.next();
                while (iterator.hasNext()) {
                    detailedTag = iterator.next();
                }

                Page<DetailedTagRestResource> detailedTagRestResourcePage = tags.transform(tagRestResourceTransformationFactory.getTransformation(repository));
                paramsMap.put("tags", detailedTagRestResourcePage);
            }
        }

        boolean permission = false;
        if (permissionService.hasRepositoryPermission(repository, Permission.REPO_WRITE)) {
            if (detailedTag != null) {
                SimpleTag tag = new SimpleTag.Builder()
                        .hash(detailedTag.getCommitHash())
                        .latestCommit(detailedTag.getCommitHash())
                        .id(detailedTag.getResolvedTagId())
                        .displayId(detailedTag.getPointsTo())
                        .build();
                RefAccessRequest createRefAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.CREATE).ref(tag).build();
                RefAccessRequest updateRefAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.UPDATE).ref(tag).build();
                RefAccessRequest deleteRefAccessRequest = new RefAccessRequest.Builder(repository, RefAccessType.DELETE).ref(tag).build();
                log.debug("Create Permission: " + Boolean.toString(refRestrictionService.hasPermission(createRefAccessRequest)));
                log.debug("Update Permission: " + Boolean.toString(refRestrictionService.hasPermission(updateRefAccessRequest)));
                log.debug("Delete Permission: " + Boolean.toString(refRestrictionService.hasPermission(deleteRefAccessRequest)));
                if (refRestrictionService.hasPermission(createRefAccessRequest) || refRestrictionService.hasPermission(updateRefAccessRequest) || refRestrictionService.hasPermission(deleteRefAccessRequest)) {
                    log.debug("User has permission!");
                    permission = true;
                } else {
                    log.debug("User has repo permission, but doesn't have branch permission!");
                    permission = false;
                }
            } else {
                permission = true;
            }
        }
        paramsMap.put("permission", permission);

        pageBuilderService.assembler().resources().requireContext("contribute.page.taglist");
        render(resp, "Contribute.Stash.Templates.TagList.tagOverview", paramsMap);
    }


    private int getStartingFrom(HttpServletRequest req) {
        String param = req.getParameter("startingFrom");
        return param != null ? Integer.parseInt(param) : 0;
    }

    @Override
    public String getResource() {
        return "be.contribute.atlassian.stash-tagging-support:tag-list-resources";
    }
}
