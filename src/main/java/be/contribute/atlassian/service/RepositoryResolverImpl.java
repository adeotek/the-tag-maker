package be.contribute.atlassian.service;

import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;


public class RepositoryResolverImpl implements RepositoryResolver {

    private final RepositoryService repositoryService;

    public RepositoryResolverImpl(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @Override
    public Repository resolveRepository(String projectKey, String repoSlug) {
        return resolveRepository(projectKey, repoSlug, false);
    }

    @Override
    public Repository resolveRepository(String projectKey, String repoSlug, boolean personal) {
        return repositoryService.getBySlug(appendUserSign(personal) + projectKey, repoSlug);
    }

    private String appendUserSign(boolean personal) {
        return personal ? "~" : "";
    }
}
