package be.contribute.atlassian.service;


import be.contribute.atlassian.DetailedTag;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.Tag;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;

public interface TagService {
    Page<DetailedTag> getTags(Repository repository, PageRequest pageRequest);

    public void createTag(String name, String message, String commitHash, Repository repository);

    public void deleteTag(String name, Repository repository);

    public Tag updateTag(String name, String message, String commitHash, Repository repository);

    public void renameTag(String originalTagName, String message, String commitHash, String newTagName,Repository repository);

    public DetailedTag getTag(String name, Repository repository);


}
