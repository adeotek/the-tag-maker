package be.contribute.atlassian.validator;


import java.util.ArrayList;
import java.util.List;

public class RestValidationResult {
    private List<RestError> errors  = new ArrayList<RestError>();

    public boolean isValid() {
        return errors.isEmpty();
    }

    public List<RestError> getErrors() {
        return errors;
    }

    public void addError(String errorMessage, String context) {
        errors.add(new RestError(errorMessage, context));
    }
}
