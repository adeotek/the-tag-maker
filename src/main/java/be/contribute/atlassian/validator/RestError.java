package be.contribute.atlassian.validator;


public class RestError {

    private String errorMessage;
    private String context;

    public RestError(String errorMessage, String context) {
        this.errorMessage = errorMessage;
        this.context = context;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
