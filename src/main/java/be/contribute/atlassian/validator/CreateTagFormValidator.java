package be.contribute.atlassian.validator;

import be.contribute.atlassian.web.CreateTagFormResult;

public interface CreateTagFormValidator {
    public void validate(CreateTagFormResult result);
}
