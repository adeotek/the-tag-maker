package be.contribute.atlassian.web;


import javax.servlet.http.HttpServletRequest;


public class CreateTagFormResult extends AbstractFormResult {

    private int repositoryId;
    private String commitId;

    private String description="";

    private String tagName="";

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTagName(String tagName) {
        this.tagName = tagName;
    }

    public CreateTagFormResult(int repositoryId, String commitId, String tagName, String description) {
        this.repositoryId = repositoryId;
        this.commitId = commitId;
        this.description = description;
        this.tagName = tagName;
    }

    public CreateTagFormResult() {
    }

    public CreateTagFormResult(HttpServletRequest req) {
        this.repositoryId = Integer.parseInt(req.getParameter("repoId"));
        this.commitId = req.getParameter("commitId");
        this.tagName = req.getParameter("tagName");
        this.description = req.getParameter("description");
    }


    public String getDescription() {
        return description;
    }

    public String getTagName() {
        return tagName;
    }

    public int getRepositoryId() {
        return repositoryId;
    }

    public void setRepositoryId(int repositoryId) {
        this.repositoryId = repositoryId;
    }

    public String getCommitId() {
        return commitId;
    }

    public void setCommitId(String commitId) {
        this.commitId = commitId;
    }

}
