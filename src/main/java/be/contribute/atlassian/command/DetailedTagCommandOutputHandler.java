package be.contribute.atlassian.command;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.PluginConstantsManager;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.enums.DescriptionStatus;
import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.repository.NoSuchTagException;
import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

import static be.contribute.atlassian.enums.DescriptionStatus.*;
import static be.contribute.atlassian.enums.TagType.*;


public class DetailedTagCommandOutputHandler implements CommandOutputHandler<DetailedTag> {
    private static final String VERSION = "Version:";
    private DetailedTag detailedTag;
    private Watchdog watchdog;
    private String tagName;
    private static final Logger log = LoggerFactory.getLogger(DetailedTagCommandOutputHandler.class);

    public DetailedTagCommandOutputHandler(String tagName) {
        this.tagName = tagName;
    }

    @Nullable
    @Override
    public DetailedTag getOutput() {
        return detailedTag;
    }

    @Override
    public void process(InputStream inputStream) throws ProcessException {
        detailedTag = new DetailedTag();
        //String line = null;
        try {
            BufferedReader in = getBufferedReader(inputStream);
            int lineCounter = 0;
            DescriptionStatus tagDescriptionStatus = TO_COME;
            String[] lines = getLines(in);
            for (String line : lines) {
                lineCounter++;
                log.debug("Reading line " + lineCounter + ": " + line);
                //Tagname
                if (lineCounter == 1 && line.startsWith("tag")) {
                    String tagName = line.substring("tag".length()).trim();
                    detailedTag.setTagName(tagName);
                    detailedTag.setTagType(ANNOTATED);
                }
                if (lineCounter == 1 && line.startsWith("commit")) {
                        readCommitHashLine(line);
                    detailedTag.setTagType(LIGHTWEIGHT);
                    detailedTag.setTagName(tagName);
                    return;
                }
                //Tagger
                if (lineCounter == 2 && line.startsWith("Tagger:")) {
                    int beginInfo = line.indexOf(":");
                    String taggerInfo = line.substring(beginInfo);

                    int emailSplitterStart = taggerInfo.lastIndexOf("<");
                    int emailSplitterEnd = taggerInfo.lastIndexOf(">");
                    String taggerName = taggerInfo.substring(1, emailSplitterStart - 1).trim();
                    String email = taggerInfo.substring(emailSplitterStart + 1, emailSplitterEnd).trim();

                    detailedTag.setTagger(new Tagger(taggerName, email));

                }
                //Date
                if (lineCounter == 3 && line.startsWith("Date:")) {
                    int beginInfo = line.indexOf(":");
                    String dateInfo = line.substring(beginInfo + 1).trim();
                    dateInfo = dateInfo.substring(4).toUpperCase();
                    DateTimeFormatter dtf = DateTimeFormat.forPattern(PluginConstantsManager.getGitDateFormat());
                    DateTime dateTime = dtf.withLocale(Locale.ENGLISH).parseDateTime(dateInfo);
                    detailedTag.setTaggedDateTime(dateTime);
                }

                //Description
                if ((tagDescriptionStatus == READING || tagDescriptionStatus == MAYBE_DONE) && line.equals(PluginConstantsManager.START_PGP)) {
                    tagDescriptionStatus = TYPE_SIGNATURE;
                    detailedTag.setTagType(SIGNED);
                }
                if (lineCounter == 4 && StringUtils.isBlank(line) && tagDescriptionStatus == TO_COME) {
                    tagDescriptionStatus = READING;
                }

                if (tagDescriptionStatus == SIGNATURE && line.equals(PluginConstantsManager.END_PGP)) {
                    tagDescriptionStatus = DONE;
                }
                if (tagDescriptionStatus== SIGNATURE) {
                    String signature = detailedTag.getSignature() + line;
                    detailedTag.setSignature(signature);
                }

                if (tagDescriptionStatus == TYPE_SIGNATURE && StringUtils.isBlank(line)) {
                  tagDescriptionStatus = SIGNATURE;
                }
                if (tagDescriptionStatus == TYPE_SIGNATURE && line.startsWith(VERSION)) {
                    String signatureVersion = line.substring(VERSION.length()).trim();
                    detailedTag.setSignatureVersion(signatureVersion);
                }
                if (lineCounter > 4 && StringUtils.isBlank(line) && tagDescriptionStatus == READING) {
                    tagDescriptionStatus = MAYBE_DONE;
                }
                //commit-hash
                if ((tagDescriptionStatus == MAYBE_DONE || tagDescriptionStatus == READING || tagDescriptionStatus == DONE) && line.startsWith("commit")) {
                    readCommitHashLine(line);
                    tagDescriptionStatus = DONE;
                }
                if ((tagDescriptionStatus == MAYBE_DONE || tagDescriptionStatus == READING) && line.startsWith("tag")) {
                    String tagName = line.substring("tag".length()).trim();
                    detailedTag.setTaggedTagName(tagName);
                    tagDescriptionStatus = DONE;
                }
                if (tagDescriptionStatus == MAYBE_DONE || tagDescriptionStatus == READING) {
                    detailedTag.addDescriptionLine(line);
                }
            }
//            if (detailedTag.getCommitHash() == null) {
//                lineCounter = 0;
//                detailedTag.setDescription(new String[]{});
//                boolean tagFilled = false;
//                for (String line : lines) {
//                    lineCounter++;
//                    if (lineCounter > 4 && (!line.startsWith("commit")) && !tagFilled) {
//                        detailedTag.addDescriptionLine(line);
//                    }
//                    if (lineCounter > 4 && line.startsWith("commit")) {
//                        readCommitHashLine(line);
//                        tagFilled = true;
//                    }
//                }
//            }
        } catch (IOException e) {
            log.error("error while parsing detailedTag.", e);
            throw new ProcessException(e);
        }
        if (StringUtils.isBlank(detailedTag.getTagName())) {
            String message = "No tag found with name " + tagName;
            KeyedMessage keyedMessage = new KeyedMessage(message,message,message);
            NoSuchTagException ex = new NoSuchTagException(keyedMessage, tagName);
            throw new ProcessException(ex);
        }
    }

    private void readCommitHashLine(String line) {
        String commitHash = line.substring("commit".length()).trim();
        detailedTag.setCommitHash(commitHash);
    }

    @Override
    public void complete() throws ProcessException {
        //To change body of implemented methods use File | Settings | File Templates.
        log.debug("DetailedTagCommandOutputHandler completed");
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        this.watchdog = watchdog;
    }

    protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
        return new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
    }

    protected String[] getLines(BufferedReader in) throws IOException {
        int c = 0;
        int line = 1;
        StringBuffer sb = new StringBuffer();
        while((c = in.read()) != -1) {
            sb.append((char) c);
        }
        String lines = sb.toString();
        //log.error("Lines received:\n" + lines);
        lines = lines.replaceAll("\\r", "");
        return lines.split("\\n");
    }
}
