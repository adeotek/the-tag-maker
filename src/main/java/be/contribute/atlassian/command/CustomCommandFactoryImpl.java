package be.contribute.atlassian.command;

import be.contribute.atlassian.DetailedTag;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.Command;
import com.atlassian.bitbucket.scm.ScmCommandBuilder;
import com.atlassian.bitbucket.scm.ScmService;

import java.util.List;


public class CustomCommandFactoryImpl implements CustomCommandFactory {
    private ScmService scmService;

    public CustomCommandFactoryImpl(ScmService scmService) {
        this.scmService = scmService;
    }

    /*
    * Will be removed in future versions, legacy from support before Stash 3.5
    */
    @Deprecated
    @Override
    public Command<Void> tag(Repository repository, String commitId, String tagName, String tagDescription) {
        ScmCommandBuilder<?> builder = scmService.createBuilder(repository);
        Command<Void> command = builder.command("tag")
                .argument("--annotate").argument(tagName)
                .argument("--message").argument(tagDescription)
                .argument(commitId)
                .exitHandler(new CreateTagExitHandler())
                .build(new TagCommandOutputHandler());


        return command;
    }

    /*
    * Will be removed in future versions, legacy from support before Stash 3.5
    */
    @Deprecated
    @Override
    public Command<List<String>> tagsForCommit(Repository repository, String commitId) {
        ScmCommandBuilder<?> scmCommandBuilder = scmService.createBuilder(repository);
        scmCommandBuilder.exitHandler(new PluginCommandErrorHandler());
        Command<List<String>> command = scmCommandBuilder.command("tag")
                .argument("--points-at").argument(commitId)
                .build(new TagListCommandOutputHandler());

        return command;
    }

    @Override
    public Command<Boolean> tagByName(Repository repository, String tagName) {
        ScmCommandBuilder<?> scmCommandBuilder = scmService.createBuilder(repository);
        scmCommandBuilder.exitHandler(new PluginCommandErrorHandler());
        Command<Boolean> command = scmCommandBuilder.command("tag")
                .argument("-l").argument(tagName)
                .build(new TagExistCommandHandler());
        return command;
    }

    @Override
    public Command<DetailedTag> tagInformation(Repository repository, String tagName) {
        ScmCommandBuilder<?> builder = scmService.createBuilder(repository);
        Command<DetailedTag> command = builder
                .exitHandler(new PluginCommandErrorHandler())
                .command("show")
                .argument("--quiet")
                .argument(tagName)
                .build(new DetailedTagCommandOutputHandler(tagName));
        return command;
    }
}
