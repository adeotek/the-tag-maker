package be.contribute.atlassian.command;


import be.contribute.atlassian.exception.TagNameException;
import com.atlassian.bitbucket.scm.CommandExitHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class CreateTagExitHandler implements CommandExitHandler{

    private static final String FATAL = "fatal:";

    @Override
    public void onCancel(@Nonnull String s, int i, @Nullable String s2, @Nullable Throwable throwable) {
        throwable.printStackTrace();
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onExit(@Nonnull String command, int exitCode, @Nullable String stdErr, @Nullable Throwable thrown) {
      if (thrown != null) {
          String errorMessage = stdErr;
          if (stdErr.startsWith(FATAL) && stdErr.endsWith("is not a valid tag name.")) {
               errorMessage = stdErr.substring(FATAL.length());
          }
          throw new TagNameException(errorMessage);
      }

    }
}
