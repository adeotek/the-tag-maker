package be.contribute.atlassian.command;

import com.atlassian.bitbucket.scm.CommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;


public class TagExistCommandHandler  implements CommandOutputHandler<Boolean> {
    private Watchdog watchdog;
    private List<String> lines = new ArrayList<String>();

    @Nullable
    @Override
    public Boolean getOutput() {
        return lines.size() > 0;
    }

    @Override
    public void process(InputStream inputStream) throws ProcessException {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line;
            while ((line = in.readLine()) != null) {
                lines.add(line);
            }
        } catch (IOException e) {
            // e.printStackTrace();
        }
    }

    @Override
    public void complete() throws ProcessException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        this.watchdog = watchdog;
    }
}
