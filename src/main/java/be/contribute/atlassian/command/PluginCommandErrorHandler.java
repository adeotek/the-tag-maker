package be.contribute.atlassian.command;

import com.atlassian.bitbucket.scm.CommandErrorHandler;
import com.atlassian.bitbucket.scm.CommandExitHandler;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.Watchdog;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.InputStream;

/**
 * Created with IntelliJ IDEA.
 * User: tomas
 * Date: 27/06/14
 * Time: 15:07
 * To change this template use File | Settings | File Templates.
 */
public class PluginCommandErrorHandler implements CommandErrorHandler, CommandExitHandler {
    @Override
    public void process(InputStream inputStream) throws ProcessException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void complete() throws ProcessException {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void setWatchdog(Watchdog watchdog) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onCancel(@Nonnull String s, int i, @Nullable String s2, @Nullable Throwable throwable) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onExit(@Nonnull String s, int i, @Nullable String s2, @Nullable Throwable throwable) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
