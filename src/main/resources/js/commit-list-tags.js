AJS.$(function () {


    AJS.$(".create-tag-button").bind("click", function (e) {
        e.preventDefault();
        var repoId = AJS.$(this).data("repo-id");
        var commitId = AJS.$(this).data("commit-id");
        window.location.replace(AJS.contextPath() + "/plugins/servlet/create-tag" + "?commitId=" + commitId + "&repoId=" + repoId);
    });

});


