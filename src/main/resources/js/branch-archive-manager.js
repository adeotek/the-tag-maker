var ArchiveBranchManager = {
        createLink: function (ctx) {
            var navbuilder = require('bitbucket/util/navbuilder');
            var urlBuilder = navbuilder.pluginServlets().path('archive-branch');
            console.log(ctx);
            if (ctx.repository) {
                urlBuilder = urlBuilder.withParams({repoId: ctx.repository.id});
            }
            if (ctx.atRevisionRef && ctx.atRevisionRef.type.id === 'branch') {
                urlBuilder = urlBuilder.withParams({branchToArchive: ctx.atRevisionRef.id});
            }  else {
                urlBuilder = urlBuilder.withParams({branchToArchive: ctx.branch.id});
            }
            return urlBuilder.build();
        },

        isDefaultBranch: function (ctx) {
            return !ctx.branch.isDefault
        },
        onDefaultBranch: function (ctx) {
            return !ctx.atRevisionRef.isDefault
        }

    }
    ;