package ut.be.contribute.atlassian.validator;

import be.contribute.atlassian.tag.rest.UpdateTagRestResource;
import be.contribute.atlassian.validator.RestValidationResult;
import be.contribute.atlassian.validator.TagRestValidator;
import be.contribute.atlassian.validator.TagRestValidatorImpl;
import com.atlassian.bitbucket.NoSuchEntityException;
import com.atlassian.bitbucket.commit.CommitRequest;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.i18n.KeyedMessage;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TagRestValidatorImplTest {
    private static final String INVALID_COMMIT_ID = "invalid";
    private static final String REPO_SLUG = "rep_1";
    private static final String PROJECT_KEY = "PROJ_1";
    private static final String CORRECT_COMMIT_ID = "correct";
    private static final String DESCRIPTION = "MESSGAE";
    private TagRestValidator tagRestValidator;
    private UpdateTagRestResource updateResource = new UpdateTagRestResource();

    @Mock
    private CommitService commitService;
    @Mock
    private Repository repository;
    @Mock
    private RepositoryService repositoryService;

    @Before
    public void setUp() {

        tagRestValidator = new TagRestValidatorImpl(commitService, repositoryService);

        updateResource.setProjectKey(PROJECT_KEY);
        updateResource.setRepoSlug(REPO_SLUG);
        updateResource.setCommitId(CORRECT_COMMIT_ID);
        updateResource.setMessage(DESCRIPTION);


        when(repositoryService.getBySlug(PROJECT_KEY, REPO_SLUG)).thenReturn(repository);
        //Om een of andere reden werkt dit niet...
        doThrow(new NoSuchEntityException(new KeyedMessage("", "", ""))).
                when(commitService).getCommit(new CommitRequest.Builder(repository, INVALID_COMMIT_ID).build());
    }

    @Test
    public void update_hasMessage() {
        updateResource.setMessage("");
        RestValidationResult result = validateUpdate();
        assertThat(result.isValid());
    }

    @Test
    public void update_hasNonEmptyMessage() {
        updateResource.setMessage(null);
        RestValidationResult result = validateUpdate();
        assertThat(result.isValid());
    }

    @Test
    public void update_hasCommitHash() {
        updateResource.setCommitId(null);
        RestValidationResult result = validateUpdate();
        assertThat(result.isValid()).isFalse();
        assertThat(result.getErrors().get(0).getContext()).isEqualTo("commitId");
        assertThat(result.getErrors().get(0).getErrorMessage()).isEqualTo("no commit specified");
    }

    @Ignore
    @Test
    public void update_hasValidCommit() {
        updateResource.setCommitId(INVALID_COMMIT_ID);
        RestValidationResult result = validateUpdate();
        assertThat(result.isValid()).isFalse();
        assertThat(result.getErrors().get(0).getContext()).isEqualTo("commitId");
        assertThat(result.getErrors().get(0).getErrorMessage()).contains("invalid commit");
    }

    private void assertMessageError() {
        RestValidationResult result = validateUpdate();
        assertThat(result.isValid()).isFalse();
        assertThat(result.getErrors().get(0).getContext()).isEqualTo("message");
        assertThat(result.getErrors().get(0).getErrorMessage()).contains("no message specified");
    }

    private RestValidationResult validateUpdate() {
        return tagRestValidator.validate(updateResource);
    }
}
