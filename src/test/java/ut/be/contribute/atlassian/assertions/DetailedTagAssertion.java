package ut.be.contribute.atlassian.assertions;


import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.enums.TagType;
import org.fest.assertions.api.AbstractAssert;
import org.fest.assertions.api.Assertions;
import org.joda.time.DateTime;

public class DetailedTagAssertion extends AbstractAssert<DetailedTagAssertion, DetailedTag> {


    public DetailedTagAssertion(DetailedTag actual) {
        super(actual, DetailedTagAssertion.class);
    }


    public DetailedTagAssertion hasName(String tagName) {
        isNotNull();

        Assertions.assertThat(actual.getTagName())
                .overridingErrorMessage("Expected tag's name to be <%s> but was <%s>", tagName, actual.getTagName())
                .isEqualTo(tagName);

        return this;
    }

    public DetailedTagAssertion hasTagger(Tagger tagger) {
        isNotNull();

        Assertions.assertThat(actual.getTagger())
                .overridingErrorMessage("Expected tagger not to be null (<%s> but was <%s>)", tagger, actual.getTagger())
                .isNotNull();

        Assertions.assertThat(actual.getTagger().getName())
                .overridingErrorMessage("Expected tagger's name to be <%s> but was <%s>", tagger.getName(), actual.getTagger().getName())
                .isEqualTo(tagger.getName());

        Assertions.assertThat(actual.getTagger().getEmail())
                .overridingErrorMessage("Expected tagger's email to be <%s> but was <%s>", tagger.getEmail(), actual.getTagger().getEmail())
                .isEqualTo(tagger.getEmail());


        return this;
    }

    public DetailedTagAssertion hasCommitHash(String commitHash) {
        isNotNull();

        Assertions.assertThat(actual.getCommitHash())
                .overridingErrorMessage("Expected tag's commit-hash to be <%s> but was <%s>", commitHash, actual.getCommitHash())
                .isEqualTo(commitHash);
        return this;
    }

    public DetailedTagAssertion isType(TagType tagType) {
        isNotNull();

        Assertions.assertThat(actual.getTagType())
                .overridingErrorMessage("Expected tag's type to be <%s> but was <%s>", tagType, actual.getTagType())
                .isEqualTo(tagType);
        return this;
    }

    public DetailedTagAssertion hasDescription(String[] description) {
        isNotNull();

        Assertions.assertThat(actual.getDescription())
                .overridingErrorMessage("Expected tag's description to be <%s> but was <%s> containing <%s> but was <%s>",description,actual.getDescription(), showDescription(description), showDescription(actual.getDescription()))
                .containsExactly(description);
        return this;
    }

    public DetailedTagAssertion hasTaggedDateTime(DateTime dateTime) {
        isNotNull();

        Assertions.assertThat(actual.getTaggedDateTime())
                .overridingErrorMessage("Expected tag's tagged DateTime to be <%s> but was <%s>", dateTime, actual.getTaggedDateTime())
                .isEqualTo(dateTime);
        return this;
    }

    public DetailedTagAssertion isComplete() {
        isNotNull();

        Assertions.assertThat(actual.getTaggedDateTime())
                .overridingErrorMessage("Expected tag's tagged DateTime not to be null")
                .isNotNull();
        Assertions.assertThat(actual.getTagName())
                .overridingErrorMessage("Expected tag's name DateTime not to be null")
                .isNotNull();
        Assertions.assertThat(actual.getTagger())
                .overridingErrorMessage("Expected tagger not to be null")
                .isNotNull();
        Assertions.assertThat(actual.getCommitHash())
                .overridingErrorMessage("Expected commit-hash not to be null")
                .isNotNull();
        Assertions.assertThat(actual.getDescription())
                .overridingErrorMessage("Expected tag's description  not to be empty")
                .isNotEmpty();
        return this;
    }

    public DetailedTagAssertion pointsToOtherTag(String otherTagName) {
        isNotNull();
        Assertions.assertThat(actual.getTaggedTagName())
                .overridingErrorMessage("Expected tag's points to tag with name <%s> but was <%s>", otherTagName, actual.getTaggedTagName())
                .isEqualTo(otherTagName);
        return this;
    }

    public DetailedTagAssertion hasSignature(String signature) {
        isNotNull();
        Assertions.assertThat(actual.getSignature())
                .overridingErrorMessage("Expected tag's signature to be  <%s> but was <%s>", signature, actual.getSignature())
                .isEqualTo(signature);
        return this;
    }

    public DetailedTagAssertion pointsTo(String pointsTo) {
        isNotNull();
        Assertions.assertThat(actual.getPointsTo())
                .overridingErrorMessage("Expected tag's points to  <%s> but was <%s>", pointsTo, actual.getPointsTo())
                .isEqualTo(pointsTo);
        return this;
    }

    public DetailedTagAssertion hasSignatureVersion(String signVersion) {
        isNotNull();
        Assertions.assertThat(actual.getSignatureVersion())
                .overridingErrorMessage("Expected tag's signature version to be <%s> but was <%s>", signVersion, actual.getSignatureVersion())
                .isEqualTo(signVersion);
        return this;
    }

    private String showDescription(String[] description) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < description.length; i++) {
            sb.append(description[i]);
        }
        return sb.toString();
    }
}
