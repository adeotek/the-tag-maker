package ut.be.contribute.atlassian.assertions;

import be.contribute.atlassian.DetailedTag;
import org.fest.assertions.api.Assertions;


public class PluginAssertions extends Assertions {
    public static DetailedTagAssertion assertThat(DetailedTag actual){
        return new DetailedTagAssertion(actual);
    }
}
