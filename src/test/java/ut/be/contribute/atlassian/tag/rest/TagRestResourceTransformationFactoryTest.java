package ut.be.contribute.atlassian.tag.rest;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.tag.rest.DetailedTagRestTransformFunction;
import be.contribute.atlassian.tag.rest.TagRestResourceTransformationFactory;
import be.contribute.atlassian.tag.rest.TagRestResourceTransformationFactoryImpl;
import com.atlassian.bitbucket.avatar.AvatarService;
import com.atlassian.bitbucket.commit.Commit;
import com.atlassian.bitbucket.commit.CommitService;
import com.atlassian.bitbucket.nav.NavBuilder;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.repository.RefService;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.git.command.tag.GitTag;
import com.atlassian.bitbucket.util.DateFormatter;
import com.atlassian.bitbucket.util.PageRequestImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.fest.assertions.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class TagRestResourceTransformationFactoryTest {
    private static final int LIMIT = 20;
    private static final int START = 0;
    private final PageRequestImpl pageRequest = new PageRequestImpl(START, LIMIT);
    private TagRestResourceTransformationFactory factory;
    private DetailedTag detailedTag;

    @Mock
    private Repository repository;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private NavBuilder navBuilder;
    @Mock
    private Project project;
    @Mock
    private CommitService commitService;
    @Mock
    private Commit commit;
    @Mock
    private AvatarService avatarService;
    @Mock
    private GitTag tag;
    @Mock
    private RefService refService;
    @Mock
    private DateFormatter dateFormatter;



    @Before
    public void setUp() {
        factory = new TagRestResourceTransformationFactoryImpl(navBuilder, commitService, avatarService, refService, dateFormatter);
    }

    @Test
    public void create_hasAllDependencies() {
        DetailedTagRestTransformFunction transformation = factory.getTransformation(repository);
        assertThat(transformation.getAvatarService()).isEqualTo(avatarService);
        assertThat(transformation.getCommitService()).isEqualTo(commitService);
        assertThat(transformation.getDateFormatter()).isEqualTo(dateFormatter);
        assertThat(transformation.getNavBuilder()).isEqualTo(navBuilder);
    }

    @Test
    public void create_hasRepository() {
        DetailedTagRestTransformFunction transformation = factory.getTransformation(repository);
        assertThat(transformation.getRepository()).isEqualTo(repository);
    }




}
