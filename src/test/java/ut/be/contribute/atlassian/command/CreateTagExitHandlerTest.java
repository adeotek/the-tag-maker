package ut.be.contribute.atlassian.command;

import be.contribute.atlassian.command.CreateTagExitHandler;
import be.contribute.atlassian.exception.TagNameException;
import com.atlassian.bitbucket.ServerException;
import com.atlassian.bitbucket.i18n.KeyedMessage;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CreateTagExitHandlerTest {
    private CreateTagExitHandler createTagExitHandler;

    @Before
    public void setUp() {
        createTagExitHandler = new CreateTagExitHandler();
    }

    @Test
    public void onExit_throwsNothingOnError() {
        createTagExitHandler.onExit("",1,"", null);
    }

    @Test(expected = TagNameException.class)
    public void onExit_throwsCustomError() {
        createTagExitHandler.onExit("",1,"", new ServerException(new KeyedMessage("","","")));
    }
}
