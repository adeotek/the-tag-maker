package ut.be.contribute.atlassian.command;

import be.contribute.atlassian.command.CustomCommandFactoryImpl;
import be.contribute.atlassian.command.DetailedTagCommandOutputHandler;
import be.contribute.atlassian.command.TagCommandOutputHandler;
import be.contribute.atlassian.command.TagListCommandOutputHandler;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.scm.Command;
import com.atlassian.bitbucket.scm.CommandExitHandler;
import com.atlassian.bitbucket.scm.ScmCommandBuilder;
import com.atlassian.bitbucket.scm.ScmService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CustomCommandFactoryImplTest {
    public static final String COMMIT_ID = "asds2d3ds3ffd4";
    public static final String TAG_NAME = "TAG.1.2.3";
    public static final String TEST_TAG = "test_tag";
    private static final String DESCRIPTION = "blah blah";
    private CustomCommandFactoryImpl customCommandFactory;
    @Mock
    private Repository repository;
    @Mock
    private ScmService scmService;
    @Mock
    private ScmCommandBuilder voidScmCommandBuilder;

    @Before
    public void setUp() {
        when(voidScmCommandBuilder.command(anyString())).thenReturn(voidScmCommandBuilder);
        when(voidScmCommandBuilder.argument(anyString())).thenReturn(voidScmCommandBuilder);
        when(voidScmCommandBuilder.exitHandler(any(CommandExitHandler.class))).thenReturn(voidScmCommandBuilder);
        when(scmService.createBuilder(any(Repository.class))).thenReturn(voidScmCommandBuilder);

        customCommandFactory = new CustomCommandFactoryImpl(scmService);

    }

    @Test
    public void tag_containsCorrectArguments() {
        customCommandFactory.tag(repository, COMMIT_ID,TAG_NAME, DESCRIPTION);
        verify(voidScmCommandBuilder).command("tag");
        verify(voidScmCommandBuilder).argument("--annotate");
        verify(voidScmCommandBuilder).argument(TAG_NAME);
        verify(voidScmCommandBuilder).argument("--message");
        verify(voidScmCommandBuilder).argument(DESCRIPTION);
        verify(voidScmCommandBuilder).build(any(TagCommandOutputHandler.class));
    }

    @Test
    public void tagsForCommit_containsCorrectArguments() {
        customCommandFactory.tagsForCommit(repository, COMMIT_ID);
        verify(voidScmCommandBuilder).command("tag");
        verify(voidScmCommandBuilder).argument("--points-at");
        verify(voidScmCommandBuilder).argument(COMMIT_ID);
        verify(voidScmCommandBuilder).build(any(TagListCommandOutputHandler.class));
    }

    @Test
    public void tagByName_containsCorrectArguments() {
        customCommandFactory.tagByName(repository,TAG_NAME);
        verify(voidScmCommandBuilder).command("tag");
        verify(voidScmCommandBuilder).argument("-l");
        verify(voidScmCommandBuilder).argument(TAG_NAME);
        verify(voidScmCommandBuilder).build(any(TagListCommandOutputHandler.class));
    }

    @Test
    public void tagInfo_containsCorrectArguments() {
        customCommandFactory.tagInformation(repository, TAG_NAME);
        verify(voidScmCommandBuilder).command("show");
        verify(voidScmCommandBuilder).argument("--quiet");
        verify(voidScmCommandBuilder).argument(TAG_NAME);
        verify(voidScmCommandBuilder).build(any(DetailedTagCommandOutputHandler.class));

    }
}
