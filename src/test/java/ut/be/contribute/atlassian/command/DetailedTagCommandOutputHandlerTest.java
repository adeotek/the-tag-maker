package ut.be.contribute.atlassian.command;

import be.contribute.atlassian.DetailedTag;
import be.contribute.atlassian.PluginConstantsManager;
import be.contribute.atlassian.Tagger;
import be.contribute.atlassian.command.DetailedTagCommandOutputHandler;
import com.atlassian.utils.process.ProcessException;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.Locale;

import static be.contribute.atlassian.enums.TagType.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class DetailedTagCommandOutputHandlerTest {
    private static final String TAGGER_NAME = "Tomas Kenis";
    private static final String TAGNAME = "v1.4.0";
    private static final String TAG_LINE = "tag " + TAGNAME;
    private static final String TAGGER_EMAIL = "tomas.kenis@contribute.be";
    private static final String TAGGER = "Tagger: " + TAGGER_NAME + " <" + TAGGER_EMAIL + ">";
    private static final String TAG_DATETIME = "Wed Jul 23 14:07:50 2014 +0200";
    private static final String TAG_DATE = "Date: " + TAG_DATETIME;
    private static final String COMMIT_AUTHOR = "Author: " + TAGGER_NAME + " <" + TAGGER_EMAIL + "> ";
    private static final String COMMIT_HASH = "db4be644ca198d6e50bce828821dfaf825384461";
    private static final String COMMIT_HASH_LINE = "commit " + COMMIT_HASH + "   ";
    private static final String TAG_DESC_1 = "First Line  ";
    private static final String TAG_DESC_2 = "Second Line  ";
    private static final String DATETIME = "Wed Jul 23 14:02:11 2014 +0200";
    private static final String COMMIT_DATE = "Date:   " + DATETIME;
    private static final String COMMIT_DESC = "some JS";
    private static final String OTHER_TAG_NAME = "otherTagName";
    private static final String SIGN_3 = "=WryJ";
    private static final String SIGN_2 = "Ki0An2JeAVUCAiJ7Ox6ZEtK+NvZAj82/";
    private static final String SIGN_1 = "iEYEABECAAYFAkmQurIACgkQON3DxfchxFr5cACeIMN+ZxLKggJQf0QYiQBwgySN";
    private static final String SIGN_VERSION = "GnuPG v1.4.8 (Darwin)";
    private static final String LINE_ENDING = "\r\n";
    private DetailedTagCommandOutputHandler detailedTagCommandOutputHandler;

    @Mock
    private BufferedReader mockBufferedReader;
    @Mock
    private InputStream inputStream;

    private Logger log = LoggerFactory.getLogger(DetailedTagCommandOutputHandlerTest.class);
    @Before
    public void setUp() throws IOException {
        final String tagString = TAG_LINE + LINE_ENDING +
                TAGGER + LINE_ENDING +
                TAG_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                TAG_DESC_1 + LINE_ENDING +
                TAG_DESC_2 + LINE_ENDING +
                "" + LINE_ENDING +
                COMMIT_HASH_LINE + LINE_ENDING +
                COMMIT_AUTHOR + LINE_ENDING +
                COMMIT_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                COMMIT_DESC + LINE_ENDING +
                "" + LINE_ENDING;
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
        };
//
//        when(mockBufferedReader.readLine()).thenReturn(TAG_LINE,
//                TAGGER,
//                TAG_DATE,
//                "",
//                TAG_DESC_1,
//                TAG_DESC_2,
//                "",
//                COMMIT_HASH_LINE,
//                COMMIT_AUTHOR,
//                COMMIT_DATE,
//                "",
//                COMMIT_DESC,
//                "",
//                null);
//        when(mockBufferedReader.read()).thenReturn(in.read());
    }

    @Test
    public void process_handleTagWithCommitRightAfterDescription() {
        final String tagString = TAG_LINE + LINE_ENDING +
                TAGGER + LINE_ENDING +
                TAG_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                TAG_DESC_1 + LINE_ENDING +
                TAG_DESC_2 + LINE_ENDING +
                COMMIT_HASH_LINE + LINE_ENDING +
                COMMIT_AUTHOR + LINE_ENDING +
                COMMIT_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                COMMIT_DESC + LINE_ENDING +
                "" + LINE_ENDING;
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
        };
        process();
        assertThat(getOutput()).hasCommitHash(COMMIT_HASH);
    }

    @Test
    public void process_handleTagDescriptionWithCommitRightAfterDescription() {
        final String tagString = TAG_LINE + LINE_ENDING +
                TAGGER + LINE_ENDING +
                TAG_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                TAG_DESC_1 + LINE_ENDING +
                TAG_DESC_2 + LINE_ENDING +
                COMMIT_HASH_LINE + LINE_ENDING +
                COMMIT_AUTHOR + LINE_ENDING +
                COMMIT_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                COMMIT_DESC + LINE_ENDING +
                "" + LINE_ENDING;
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
        };
        process();
        //assertEquals(getOutput().getDescription(), new String[]{TAG_DESC_1, TAG_DESC_2});
        assertThat(getOutput()).hasDescription(new String[]{TAG_DESC_1, TAG_DESC_2});
    }

    @Test
    public void process_hasCorrectTagName() {
        process();
        assertThat(getOutput()).hasName(TAGNAME);
    }

    @Test
    public void process_hasCorrectTagger() {
        process();
        assertThat(getOutput()).hasTagger(new Tagger(TAGGER_NAME, TAGGER_EMAIL));
    }

    @Test
    public void process_hasTaggedDateTime() {
        process();
        int beginInfo = TAG_DATE.indexOf(":");
        String dateInfo = TAG_DATE.substring(beginInfo + 1).trim();
        dateInfo = dateInfo.substring(4).toUpperCase();
        DateTimeFormatter dtf = DateTimeFormat.forPattern(PluginConstantsManager.getGitDateFormat());
        DateTime dateTime = dtf.withLocale(Locale.ENGLISH).parseDateTime(dateInfo);

        assertThat(getOutput()).hasTaggedDateTime(dateTime);
    }

    @Test
    public void process_hasCorrectDescription() {
        process();
        assertThat(getOutput()).hasDescription(new String[]{TAG_DESC_1, TAG_DESC_2});
    }

    @Test
    public void process_isCorrectType() {
        process();
        assertThat(getOutput()).isType(ANNOTATED);
    }

    @Test
    public void process_hasCorrectCommitHash() {
        process();
        assertThat(getOutput()).hasCommitHash(COMMIT_HASH);
    }

    @Test
    public void process_createsCompleteDetailedTag() {
        process();
        assertThat(getOutput()).isComplete();
    }

    @Test
    public void output_isNotNull() {
        process();
        assertThat(getOutput()).isNotNull();
    }

    @Test(expected = ProcessException.class)
    public void process_IOExceptionAsProcesException() throws IOException, ProcessException {
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return mockBufferedReader;
            }
        };
        doThrow(new IOException()).when(mockBufferedReader).read();
        detailedTagCommandOutputHandler.process(inputStream);
    }

    @Test
    public void process_tag_pointsToTag() throws IOException {
        final String tagString = TAG_LINE  + LINE_ENDING +
                TAGGER + LINE_ENDING +
                TAG_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                TAG_DESC_1 + LINE_ENDING +
                TAG_DESC_2 + LINE_ENDING +
                "" + LINE_ENDING +
                "tag " + OTHER_TAG_NAME  + LINE_ENDING +
                TAGGER  + LINE_ENDING +
                TAG_DATE + LINE_ENDING +
                COMMIT_HASH_LINE + LINE_ENDING +
                COMMIT_AUTHOR + LINE_ENDING +
                COMMIT_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                COMMIT_DESC + LINE_ENDING +
                "" + LINE_ENDING;
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
        };
//        when(mockBufferedReader.readLine()).thenReturn(TAG_LINE,
//                TAGGER,
//                TAG_DATE,
//                "",
//                TAG_DESC_1,
//                TAG_DESC_2,
//                "",
//                "tag " + OTHER_TAG_NAME,
//                TAGGER,
//                TAG_DATE,
//                COMMIT_HASH_LINE,
//                COMMIT_AUTHOR,
//                COMMIT_DATE,
//                "",
//                COMMIT_DESC,
//                "",
//                null);
        process();
        assertThat(getOutput()).pointsToOtherTag(OTHER_TAG_NAME);
    }

    @Test
    public void process_setSignatureCorrectly() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).hasSignature(SIGN_1 + SIGN_2 + SIGN_3);
    }

    @Test
    public void process_setSignedTagTypeCorrectly() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).isType(SIGNED);
    }

    @Test
    public void process_signedTagHasCorrectDescription() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).hasDescription(new String[]{TAG_DESC_1, TAG_DESC_2});
    }

    @Test
    public void process_setSignatureVersionCorrectly() throws IOException {
        signedTag();
        process();
        assertThat(getOutput()).hasSignatureVersion(SIGN_VERSION);
    }

    private void signedTag() throws IOException {
        final String tagString = TAG_LINE+ LINE_ENDING +
                TAGGER+ LINE_ENDING +
                TAG_DATE+ LINE_ENDING +
        ""+ LINE_ENDING +
                TAG_DESC_1+ LINE_ENDING +
                TAG_DESC_2+ LINE_ENDING +
                "-----BEGIN PGP SIGNATURE-----"+ LINE_ENDING +
                "Version: " + SIGN_VERSION+ LINE_ENDING +
                ""+ LINE_ENDING +
                SIGN_1+ LINE_ENDING +
                SIGN_2+ LINE_ENDING +
                SIGN_3+ LINE_ENDING +
                "-----END PGP SIGNATURE-----"+ LINE_ENDING +
                COMMIT_HASH_LINE+ LINE_ENDING +
                COMMIT_AUTHOR+ LINE_ENDING +
                COMMIT_DATE+ LINE_ENDING +
                ""+ LINE_ENDING +
                COMMIT_DESC+ LINE_ENDING +
                "" + LINE_ENDING ;
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
        };
        when(mockBufferedReader.readLine()).thenReturn(TAG_LINE,
                TAGGER,
                TAG_DATE,
                "",
                TAG_DESC_1,
                TAG_DESC_2,
                "-----BEGIN PGP SIGNATURE-----",
                "Version: " + SIGN_VERSION,
                "",
                SIGN_1,
                SIGN_2,
                SIGN_3,
                "-----END PGP SIGNATURE-----",
                COMMIT_HASH_LINE,
                COMMIT_AUTHOR,
                COMMIT_DATE,
                "",
                COMMIT_DESC,
                "",
                null);
    }

    @Test
    public void lightweigtTag_isCorrect() throws IOException {
        lightweight();
        process();
        assertThat(getOutput())
                .hasCommitHash(COMMIT_HASH)
                .isType(LIGHTWEIGHT);
    }

    @Test
    public void lightweight_hasNoCommittedDate() throws IOException {
        lightweight();
        process();
        assertThat(getOutput())
                .hasTaggedDateTime(null);
    }

    @Test
    public void lightWeight_hasName() throws IOException {
        lightweight();
        process();
        assertThat(getOutput())
                .hasName(TAGNAME);
    }

    @Test(expected = ProcessException.class)
    public void noTag_throwsException() throws IOException, ProcessException {
        final String tagString = "";
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
            protected String[] getLines(BufferedReader in) throws IOException {
                throw new IOException();
            }
        };
        detailedTagCommandOutputHandler.process(inputStream);
    }

    private void lightweight() throws IOException {
        final String tagString = COMMIT_HASH_LINE + LINE_ENDING +
                COMMIT_AUTHOR + LINE_ENDING +
                COMMIT_DATE + LINE_ENDING +
                "" + LINE_ENDING +
                COMMIT_DESC + LINE_ENDING;
        final InputStream in = new ByteArrayInputStream(tagString.getBytes());
        detailedTagCommandOutputHandler = new DetailedTagCommandOutputHandler(TAGNAME) {
            protected BufferedReader getBufferedReader(InputStream inputStream) throws UnsupportedEncodingException {
                return new BufferedReader(new InputStreamReader(in, "UTF-8"));
            }
        };
        when(mockBufferedReader.readLine()).thenReturn(COMMIT_HASH_LINE,
                COMMIT_AUTHOR,
                COMMIT_DATE,
                "",
                COMMIT_DESC,
                null);
    }

    private DetailedTag getOutput() {
        return detailedTagCommandOutputHandler.getOutput();
    }

    private void process() {
        try {
            detailedTagCommandOutputHandler.process(inputStream);
        } catch (ProcessException e) {
            log.error("Process exception", e);
            fail();
        }
    }

}
