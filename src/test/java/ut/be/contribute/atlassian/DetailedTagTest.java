package ut.be.contribute.atlassian;


import be.contribute.atlassian.DetailedTag;
import org.junit.Before;
import org.junit.Test;

import static ut.be.contribute.atlassian.assertions.PluginAssertions.assertThat;

public class DetailedTagTest {
    private static final String LINE1 = "description1";
    private static final String LINE2 = "description2";
    private static final String COMMIT_HASH = "a1b2c3";
    private static final String OTHER_TAG = "otherTag";
    private DetailedTag detailedTag;
    @Before
    public void setUp() {
         detailedTag = new DetailedTag();
    }

    @Test
    public void addDescriptionLine_isCorrect() {
        detailedTag.addDescriptionLine(LINE1);
        detailedTag.addDescriptionLine(LINE2);
        assertThat(detailedTag).hasDescription(new String[]{LINE1, LINE2});
    }

    @Test
    public void pointsTo_commitHash() {
        detailedTag.setCommitHash(COMMIT_HASH);
        detailedTag.setTaggedTagName(null);
        assertThat(detailedTag).pointsTo(COMMIT_HASH);
    }

    @Test
    public void pointsTo_OtherTag() {
        detailedTag.setCommitHash(COMMIT_HASH);
        detailedTag.setTaggedTagName(OTHER_TAG);
        assertThat(detailedTag).pointsTo(OTHER_TAG);
    }
}
