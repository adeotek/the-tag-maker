package it.be.contribute.atlassian;


import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class SeleniumTestToDeleteScreenTest {

    @Test
    public void workGVD() {
        BitbucketTestedProduct s = TestedProductFactory.create(BitbucketTestedProduct.class);
        BitbucketLoginPage loginPage = s.visit(BitbucketLoginPage.class);
        loginPage.login("admin", "admin", BitbucketLoginPage.class);
    }
}
