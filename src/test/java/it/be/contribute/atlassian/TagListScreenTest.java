package it.be.contribute.atlassian;


import be.contribute.atlassian.pageobjects.elements.EditTagDialog;
import be.contribute.atlassian.pageobjects.elements.TagList;
import be.contribute.atlassian.pageobjects.page.tag.TagListPage;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class TagListScreenTest extends AbstractTagScreenTest {


    @Test
    public void tagList_hasTags() {
        TagListPage tagListPage = goToTagListPage();
        List<TagList.TagListEntry> rows = tagListPage.getTagList().getRows();
        assertThat(rows).isNotEmpty();
    }

    @Test
    public void deleteTag_deletes() {
        String tagname = "tag_to_delete";

        createTag(tagname, "please delete me", "2d8897c9ac29ce42c3442cf80ac977057045e7f6");

        TagListPage tagListPage = (TagListPage) goToTagListPage()
                .getTagList()
                .getEntryForTag(tagname)
                .clickDeleteTag()
                .clickConfirmAndBind();
        asssertTagDeleted(tagname, tagListPage);
    }

    @Test
    public void edit_description_correct() {
        String editTagName = "tag_to_edit";
        String newDescription = "a new description";
        createTag(editTagName, "please edit this", "9c05f43f859375e392d90d23a13717c16d0fdcda");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setDescription(newDescription);
        TagListPage tagListPage = editTagDialog.clickConfirmAndBind();

        String currentDescription = tagListPage.getTagList().getEntryForTag(editTagName).getDescription();

        assertThat(currentDescription).isEqualTo(newDescription);
    }

    @Test
    public void edit_commit_hash_correct() {
        String editTagName = "tag_to_edit_hash";
        String originalCommitId = "53979b3304bece75a3852a3d250dbaed64d45430";
        String newCommitId = "05c78271ed4d7a158fe6789f6b27e3a47631faba";

        createTag(editTagName, "please edit this commit", originalCommitId);

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setCommitId(newCommitId);
        TagListPage tagListPage = editTagDialog.clickConfirmAndBind();

        String currentCommit = tagListPage.getTagList().getEntryForTag(editTagName).getCommitText();
        assertThat(newCommitId).startsWith(currentCommit);
    }

    @Test
    public void edit_commit_ref_correct() {
        String editTagName = "tag_to_edit_ref";
        createTag(editTagName, "please edit this ref", "c2608f5dff150e2b26b4b68d9e22369581b39b0c");
        String refTagName = "ref_tag_name";

        createTag(refTagName, "please edit this", "4f9290ae1a9fcde7acd56664a22cc65eb76540f3");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setCommitId(refTagName);
        TagListPage tagListPage = editTagDialog.clickConfirmAndBind();

        String currentRef = tagListPage.getTagList().getEntryForTag(editTagName).getCommitText();
        assertThat(refTagName).isEqualTo(currentRef);
    }

    @Test
    public void edit_commit_ref_empty() {
        String editTagName = "tag_to_edit_ref_empty";
        createTag(editTagName, "please edit this ref", "c2608f5dff150e2b26b4b68d9e22369581b39b0c");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setCommitId("");

        EditTagDialog dialogWithError = editTagDialog.clickConfirmAndExpectErrors();
        assertThat(dialogWithError.getCommitInputErrorMessage()).isEqualTo("no commit specified");
    }

    @Test
    public void edit_commit_ref_incorrect() {
        String editTagName = "tag_to_edit_ref_incorrect";
        createTag(editTagName, "please edit this ref", "c2608f5dff150e2b26b4b68d9e22369581b39b0c");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setCommitId("holleke bolleke repel sollek holleke bolleke knol");

        EditTagDialog dialogWithError = editTagDialog.clickConfirmAndExpectErrors();
        assertThat(dialogWithError.getCommitInputErrorMessage()).isEqualTo("invalid commit");
    }

    @Test
    public void edit_description_empty() {
        String editTagName = "tag_to_edit_desc_empty";
        createTag(editTagName, "please edit this ref", "c2608f5dff150e2b26b4b68d9e22369581b39b0c");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setDescription(null);
        EditTagDialog dialogWithError = editTagDialog.clickConfirmAndExpectErrors();
        assertThat(dialogWithError.getDescriptionInputErrorMessage()).isEqualTo("no message specified");
    }

    @Test
    public void edit_dialog_correctValues() {
        String editTagName = "tag_values_check";
        String description = "check this value";
        String commitId = "c2608f5dff150e2b26b4b68d9e22369581b39b0c";
        createTag(editTagName, description, commitId);

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        assertThat(editTagDialog.getDescription()).isEqualTo(description);
        assertThat(commitId).startsWith(editTagDialog.getCommitId());
    }

    @Test
    public void edit_after_edit() {
        String editTagName = "edit_after_edit";
        createTag(editTagName, "please edit this ref", "c2608f5dff150e2b26b4b68d9e22369581b39b0c");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setDescription("new description");
        TagListPage tagListPage = editTagDialog.clickConfirmAndBind();
        tagListPage.getTagList().getEntryForTag(editTagName).clickEditTag();
    }

    @Test
    public void delete_after_edit() {
        String editTagName = "delete_after_edit";
        createTag(editTagName, "please edit this ref", "c2608f5dff150e2b26b4b68d9e22369581b39b0c");

        EditTagDialog editTagDialog = goToTagListPage().getTagList().getEntryForTag(editTagName).clickEditTag();

        editTagDialog.setDescription("new description for a tag to be removed");
        TagListPage tagListPage = (TagListPage) editTagDialog
                .clickConfirmAndBind()
                .getTagList()
                .getEntryForTag(editTagName)
                .clickDeleteTag()
                .clickConfirmAndBind();

        asssertTagDeleted(editTagName, tagListPage);

    }

    @Test
    public void deleteReferencingTag_notBreaksUiAfterRefresh() {
        goToTagListPage()
                .getTagList()
                .getEntryForTag("signed_tag")
                .clickDeleteTag()
                .clickConfirmAndBind();
        TagListPage refreshedPage = BITBUCKET.visit(TagListPage.class, getProjectKey(), getRepoSlug());
        refreshedPage.isHere();
    }


    private void asssertTagDeleted(String tagname, TagListPage tagListPage) {
        List<TagList.TagListEntry> entries = tagListPage.getTagList().getRows();
        for (TagList.TagListEntry entry : entries) {
            assertThat(entry.getTagName()).isNotEqualTo(tagname);
        }
    }


}
