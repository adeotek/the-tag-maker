package it.be.contribute.atlassian;

import be.contribute.atlassian.pageobjects.page.CustomCommitPage;
import be.contribute.atlassian.pageobjects.page.tag.CreateTagPage;
import com.atlassian.webdriver.bitbucket.element.CommitList;
import com.atlassian.webdriver.bitbucket.page.CommitListPage;
import com.atlassian.webdriver.bitbucket.page.FileBrowserPage;
import org.junit.Test;

import java.util.List;

import static org.fest.assertions.api.Assertions.assertThat;

public class CreateTagScreenTest extends AbstractTagScreenTest{

    @Test
    public void canCreateTag() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("tag_test_1");
        createTagPage.setDescription("rzere oizejioez eizeoijzeoi ezo iezioez joezj");
        CustomCommitPage customCommitPage = createTagPage.submitCorrectInput();
        assertThat(customCommitPage.isHere());
    }

    @Test
    public void createDuplicateTag() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("signed_tag");
        createTagPage.setDescription("rzere oizejioez eizeoijzeoi ezo iezioez joezj");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("this name is already used for a tag");
    }

    @Test
    public void createWithoutTagName() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setDescription("just a description");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("please provide a name for the tag");
    }

    @Test
    public void createWithoutDescription() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("tag_name");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("please provide a description for the tag");
    }

    @Test
    public void createWithEmptyForm() {
        CreateTagPage createTagPage = goToCreateTag();
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors(
                "please provide a name for the tag",
                "please provide a description for the tag"
        );
    }

    @Test
    public void createIncorrect_space() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("tag name");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot contain spaces");
    }

    @Test
    public void createIncorrect_startWithDot() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName(".dot");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot start with a .");
    }

    @Test
    public void createIncorrect_containsDoubleDot() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("tag..name");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot contain a double dot (..)");
    }

    @Test
    public void createIncorrect_endsWithSlash() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("slash/");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot end with a /");
    }

    @Test
    public void createIncorrect_containsBackSlash() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("backSlash\\");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot contain a \\");
    }

    @Test
    public void createIncorrect_endsWithDotLock() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("file.lock");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot end with .lock");
    }

    @Test
    public void createIncorrect_containsColon() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("frigo:box");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot contain a :");
    }

    @Test
    public void createIncorrect_containsTilde() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("~home");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot contain a ~");
    }

    @Test
    public void createIncorrect_containsPartyHat() {
        CreateTagPage createTagPage = goToCreateTag();
        createTagPage.setTagName("party^hat");
        createTagPage.setDescription("just a placeholder discription");
        CreateTagPage errorTagPage = createTagPage.submitIncorrect();
        errorTagPage.assertHasErrors("name of the tag cannot contain a ^");
    }

    private CreateTagPage goToCreateTag() {
        FileBrowserPage repoPage = goToRepositoryPage();

        CommitListPage commitListPage = repoPage.goToCommitsPage();
        String projectKey = commitListPage.getProjectKey();
        String repoSlug = commitListPage.getSlug();

        List<CommitList.CommitListEntry> commitListEntries = commitListPage.waitUntilCommitsTableLoaded().getCommitList().getRows();
        CommitList.CommitListEntry entry = commitListEntries.get(0);
        String csid = entry.getCommitId();

        CustomCommitPage changesetPage = BITBUCKET.visit(CustomCommitPage.class, projectKey, repoSlug, csid);
        return changesetPage.clickCreateTag();
    }


}
