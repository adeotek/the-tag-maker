package it.be.contribute.atlassian;

import be.contribute.atlassian.pageobjects.TagListRepositoryNavigationWrapper;
import be.contribute.atlassian.pageobjects.page.tag.CreateTagPage;
import be.contribute.atlassian.pageobjects.page.tag.TagListPage;
import com.atlassian.bitbucket.test.BaseRetryingFuncTest;
import com.atlassian.bitbucket.test.runners.FuncTestSplitterRunner;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.bitbucket.page.BitbucketHomePage;
import com.atlassian.webdriver.bitbucket.page.BitbucketLoginPage;
import com.atlassian.webdriver.bitbucket.page.FileBrowserPage;
import com.atlassian.webdriver.bitbucket.page.ProjectOverviewPage;
import com.atlassian.webdriver.bitbucket.BitbucketTestedProduct;
import com.atlassian.webdriver.testing.rule.SessionCleanupRule;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import com.atlassian.webdriver.testing.rule.WindowSizeRule;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

import java.util.List;
import static com.atlassian.bitbucket.test.TestRuleOrderingSyntax.outerRules;


@RunWith(FuncTestSplitterRunner.class)
public class AbstractTagScreenTest extends BaseRetryingFuncTest {
    protected   String getRepoSlug (){
        return "rep_1";
    }

    protected   String getRepoName (){
        return "rep_1";
    }
    protected   String getProjectKey (){
        return "PROJECT_1";
    }

    protected   String getProjectName (){
        return "Project 1";
    }

    @BeforeClass
    public static void setUpClass() {
        BitbucketLoginPage loginPage = BITBUCKET.visit(BitbucketLoginPage.class);
        loginPage.login("admin", "admin", BitbucketHomePage.class);
        customBeforeClassAfterLogin();
    }

    protected static void customBeforeClassAfterLogin() {

    }

    protected FileBrowserPage goToRepositoryPage() {
        BitbucketHomePage homePage = BITBUCKET.visit(BitbucketHomePage.class);
        ProjectOverviewPage projectOverviewPage = homePage.getProjectTable().findProjectByName(getProjectName()).goToProjectPage();
        return projectOverviewPage.getRepositoryTable().findRepositoryByName(getRepoName()).goToRepositoryPage();
    }

    protected PageBinder getPageBinder() {
        return BITBUCKET.getPageBinder();
    }

    protected TagListPage goToTagListPage() {
        FileBrowserPage fileBrowserPage = goToRepositoryPage();
        TagListRepositoryNavigationWrapper wrapper = new TagListRepositoryNavigationWrapper(fileBrowserPage, getPageBinder());
        return wrapper.goToTagListPage();
    }

    /*@AfterClass
    public static void tearDown() {
        BITBUCKET.visit(DoLogoutPage.class);
    }*/


    protected static final BitbucketTestedProduct BITBUCKET = TestedProductFactory.create(BitbucketTestedProduct.class);

    @AfterClass
    public static void clearBrowserCookies() {
        BITBUCKET.getTester().getDriver().manage().deleteAllCookies();
    }

    @Override
    protected List<TestRule> getRetryRuleChain() {
        return outerRules(
                new WebDriverScreenshotRule(),
                new WindowSizeRule(),
                new SessionCleanupRule()
        ).around(super.getRetryRuleChain());
    }

    protected void createTag(String tagName, String description, String commitId) {
        CreateTagPage createTagPage = BITBUCKET.visit(CreateTagPage.class, getProjectKey(), getRepoSlug(), commitId);
        createTagPage.setDescription(description);
        createTagPage.setTagName(tagName);
        createTagPage.submit();
    }
}
