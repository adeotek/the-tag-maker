package be.contribute.atlassian.pageobjects;


import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.bitbucket.element.NavItem;
import com.atlassian.webdriver.bitbucket.page.BaseRepositoryPage;
import be.contribute.atlassian.pageobjects.page.tag.*;

public class TagListRepositoryNavigationWrapper extends BaseRepositoryPageNavigationWrapper {

    private final BaseRepositoryPage baseRepositoryPage;
    private final boolean personal;

    public TagListRepositoryNavigationWrapper(BaseRepositoryPage baseRepositoryPage, PageBinder pageBinder) {
        this(baseRepositoryPage, pageBinder, false);

    }

    public TagListRepositoryNavigationWrapper(BaseRepositoryPage baseRepositoryPage, PageBinder pageBinder, boolean personal) {
        super(pageBinder);
        this.baseRepositoryPage = baseRepositoryPage;
        this.personal = personal;
    }

    public TagListPage goToTagListPage() {
        NavItem item = baseRepositoryPage.getItems().getItem("tag-list-nav");
        return navigateToWithPluginItem(item, TagListPage.class, baseRepositoryPage.getProjectKey(), baseRepositoryPage.getSlug(), personal);
    }
}
