package be.contribute.atlassian.pageobjects.page;

import be.contribute.atlassian.pageobjects.page.tag.CreateTagPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.bitbucket.element.UnifiedDiffFileContent;
import com.atlassian.webdriver.bitbucket.page.CommitPage;
import org.openqa.selenium.By;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;


public class CustomCommitPage extends CommitPage<CustomCommitPage,UnifiedDiffFileContent> {
    @ElementBy(className ="file-content", pageElementClass = UnifiedDiffFileContent.class)
    protected UnifiedDiffFileContent fileContent;

    @ElementBy(className = "create-tag")
    private PageElement createTagLink;

    private  boolean personal = false;


    public CustomCommitPage(String projectKey, String repoSlug, String csid) {
        this(projectKey, repoSlug, csid, null, null);
    }

    public CustomCommitPage(String projectKey, String repoSlug, String csid, boolean personal) {
        this(projectKey, repoSlug, csid, null, null);
        this.personal = personal;
    }

    public CustomCommitPage(String projectKey, String repoSlug, String csid, String parentId) {
        this(projectKey, repoSlug, csid, parentId, null);
    }

    public CustomCommitPage(String projectKey, String repoSlug, String csid, String parentId, String changePath) {
        super(projectKey, repoSlug, csid, parentId, changePath, CustomCommitPage.class);
    }

    @Override
    public UnifiedDiffFileContent getFileContent() {
        return fileContent;
    }

    public CreateTagPage clickCreateTag() {
        waitUntilTrue(elementFinder.find(By.className("create-tag")).withTimeout(TimeoutType.SLOW_PAGE_LOAD).timed().isVisible());
        createTagLink.click();
        return pageBinder.bind(CreateTagPage.class, projectKey, repoSlug, commitId, personal);
    }

    @Override
    public String getUrl() {
        String typePath = "/projects/";
        if (personal) {
            typePath ="/users/";
        }
        String url = typePath + projectKey + "/repos/" + repoSlug + "/commits/" + this.commitId;

        if (parentId != null) {
            url += "?to=" + parentId;
        }

        if (changePath != null) {
            url += "#" + changePath;
        }

        return url;
    }
}
