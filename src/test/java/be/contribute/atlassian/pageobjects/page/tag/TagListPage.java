package be.contribute.atlassian.pageobjects.page.tag;


import be.contribute.atlassian.pageobjects.elements.TagList;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.bitbucket.page.BitbucketPage;

public class TagListPage extends BitbucketPage {


    @ElementBy(cssSelector = "table.aui")
    private PageElement tableElement;

    private final String projectKey;
    private final String repoSlug;
    private final boolean personal;

    public TagListPage(String projectKey, String repoSlug) {
        this(projectKey, repoSlug, false);
    }

    public TagListPage(String projectKey, String repoSlug, boolean personal) {
        this.projectKey = projectKey;
        this.repoSlug = repoSlug;
        this.personal = personal;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public String getRepoSlug() {
        return repoSlug;
    }

    @Override
    public String getUrl() {
        String path = "/plugins/servlet/view-tags?projKey=" + projectKey + "&repo=" + repoSlug;
        if (personal) {
            path += "&personal=true";
        }
        return path;
    }

    public TagList getTagList() {
        return  pageBinder.bind(TagList.class, tableElement, this);
    }
}
